package uk.nhs.nhsbsa.nhsnumber;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Set;

import jakarta.validation.ConstraintViolation;

import uk.nhs.nhsbsa.nhsnumber.annotation.NhsNumber;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class NhsNumberValidatorTest {

    private ConstraintValidator<NhsNumberFixture> validator = new ConstraintValidator<>();
    
    private NhsNumberFixture fixture;

    @BeforeEach
    void setup() {
        fixture = new NhsNumberFixture();
    }

    @Test
    void isValidWhenNull() {
        assertTrue(isConstraintValid(null));
    }

    @Test
    void isValidWhenEmpty() {
        assertTrue(isConstraintValid(""));
    }

    @Test
    void isValidWhenBlank() {
        assertTrue(isConstraintValid(" "));
    }

    @Test
    void isInvalidWhenNotLength10() {
        assertFalse(isConstraintValid("0"));
        assertFalse(isConstraintValid("000000000"));
        assertFalse(isConstraintValid("00000000000"));
    }

    @Test
    void isInvalidWhenNonNumeric() {
        assertFalse(isConstraintValid("QWERTYUIOP"));
    }

    /**
     * All zeroes is a valid checksum, but indicates a 'pending' number.
     */
    @Test
    void isInvalidWhenAllZeroes() {
        assertFalse(isConstraintValid("0000000000"));
    }

    @Test
    void isInvalidWhenInvalidChecksum() {
        //checksum for 987654321 is 0
        assertFalse(isConstraintValid("9876543211"));
        assertFalse(isConstraintValid("9876543212"));
        assertFalse(isConstraintValid("9876543213"));
        assertFalse(isConstraintValid("9876543214"));
        assertFalse(isConstraintValid("9876543215"));
        assertFalse(isConstraintValid("9876543216"));
        assertFalse(isConstraintValid("9876543217"));
        assertFalse(isConstraintValid("9876543218"));
        assertFalse(isConstraintValid("9876543219"));
    }

    /**
     * Sample NHS numbers taken from generator site.
     */
    @Test
    void isValidWhenGeneratedNumbers() {
        assertTrue(isConstraintValid("5165713040"));
        assertTrue(isConstraintValid("5854901706"));
        assertTrue(isConstraintValid("1182773354"));
        assertTrue(isConstraintValid("1819005607"));
        assertTrue(isConstraintValid("2505701822"));
        assertTrue(isConstraintValid("9648649758"));
        assertTrue(isConstraintValid("1248146123"));
        assertTrue(isConstraintValid("2684939829"));
        assertTrue(isConstraintValid("0547185472"));
        assertTrue(isConstraintValid("1317935330"));
    }

    private boolean isConstraintValid(String nhsNumber) {
        fixture.setNhsNumber(nhsNumber);
        Set<ConstraintViolation<NhsNumberFixture>> constraintViolations = validator.validate(fixture);
        return !validator.hasError(constraintViolations, "nhsNumber");
    }

    /**
     * Fixture class to test validation mechanics for NHS Number;
     */
    static class NhsNumberFixture {

        @NhsNumber
        private String nhsNumber;

        String getNhsNumber() {
            return nhsNumber;
        }

        void setNhsNumber(String nhsNumber) {
            this.nhsNumber = nhsNumber;
        }
    }

}
