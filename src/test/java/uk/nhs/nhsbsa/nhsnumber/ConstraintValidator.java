package uk.nhs.nhsbsa.nhsnumber;

import java.util.Iterator;
import java.util.Set;

import jakarta.validation.ConstraintViolation;

import org.hibernate.validator.HibernateValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

public class ConstraintValidator<T> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConstraintValidator.class);

    private static LocalValidatorFactoryBean localValidatorFactory;
    static {
        localValidatorFactory = new LocalValidatorFactoryBean();
        localValidatorFactory.setProviderClass(HibernateValidator.class);
        localValidatorFactory.afterPropertiesSet();
    }

    public Set<ConstraintViolation<T>> validate(T object) {
        return localValidatorFactory.validate(object);
    }

    public boolean isValid(T object) {
        Set<ConstraintViolation<T>> constraintViolations = validate(object);
        boolean result = constraintViolations.isEmpty();
        if (!result) {
            LOGGER.info("{} is invalid: {}", object, constraintViolations);
        }
        return result;
    }

    public boolean hasError(T object, String errorMessage) {
        Set<ConstraintViolation<T>> constraintViolations = validate(object);
        return hasError(constraintViolations, errorMessage);
    }

    public boolean hasError(Set<ConstraintViolation<T>> constraintViolations, String errorMessage) {
        if (constraintViolations == null) {
            return false;
        }
        Iterator<ConstraintViolation<T>> errors = constraintViolations.iterator();
        while (errors.hasNext()) {
            ConstraintViolation<?> error = errors.next();
            if (error.getPropertyPath().toString().equals(errorMessage)) {
                return true;
            }
        }
        return false;
    }
}
