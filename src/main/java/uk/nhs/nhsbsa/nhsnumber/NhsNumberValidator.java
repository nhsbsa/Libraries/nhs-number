package uk.nhs.nhsbsa.nhsnumber;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.nhs.nhsbsa.nhsnumber.annotation.NhsNumber;

public class NhsNumberValidator implements ConstraintValidator<NhsNumber, String> {

    private static final Logger LOGGER = LoggerFactory.getLogger(NhsNumberValidator.class);
    private static final String PENDING = "0000000000";

    @Override
    public void initialize(NhsNumber constraintAnnotation) {
        // no-op
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {

        // empty is valid
        if (isBlank(value)) {
            return true;
        }

        String val = value.trim();

        // 0000000000 indicates a pending NHS#
        if (PENDING.equals(val)) {
            LOGGER.debug("NHS# is 'pending': {}", value);
            return false;
        }

        // check length
        char[] chars = val.toCharArray();
        if (chars.length != 10) {
            LOGGER.debug("NHS# must be 10 characters: {}", value);
            return false;
        }

        int[] digits = new int[chars.length];
        for (int i = 0; i < digits.length; i++) {
            if (Character.isDigit(chars[i])) {
                digits[i] = Character.getNumericValue(chars[i]);
            } else {
                LOGGER.debug("NHS# must be numeric: {}", value);
                return false;
            }
        }
        return checksum(value, digits);
    }

    private boolean isBlank(String value) {
        return value == null || value.trim().length() == 0;
    }

    private boolean checksum(String value, int[] digits) {

        boolean result;
        
        int actualChecksum = digits[digits.length - 1];
        int checksum = checksum(digits);

        result = checksum == actualChecksum;
        if (!result) {
            LOGGER.debug("NHS# checksum expected={}, actual={}: {}", checksum, actualChecksum, value );
        }

        return result;
    }

    private int checksum(int[] digits) {
        
        int sum = 0;
        for (int i = 1; i < digits.length; i++) {
            sum += (11 - i) * digits[i - 1];
        }
        int remainder = sum % 11;
        int checksum = 11 - remainder;
        if (checksum == 11) {
            checksum = 0;
        }
        LOGGER.debug("NHS# checksum: {}", checksum);
        return checksum;
    }
}
