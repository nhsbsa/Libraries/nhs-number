package uk.nhs.nhsbsa.nhsnumber.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import uk.nhs.nhsbsa.nhsnumber.NhsNumberValidator;

/**
 * Annotation to validate an NHS Number.
 * Must be 10 digits with no formatting.
 * First 9 digits create checksum 10th digit using formula:
 * calculated checksum = 11 - ((sum(digit * 11 - position)) % 11)
 * digit is the digit value
 * position is the digit position starting from left at position 1
 * if calculated checksum == 10, NHS# is invalid
 * if calculated checksum == 11, checksum = 0
 */
@Documented
@Constraint(validatedBy = NhsNumberValidator.class)
@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface NhsNumber {

    String message() default "{NhsNumber}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}